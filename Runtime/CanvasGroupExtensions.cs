﻿using UnityEngine;

public static class CanvasGroupExtensions
{
    public static void Hide(this CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
    }

    public static void Show(this CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 1;
    }
}
