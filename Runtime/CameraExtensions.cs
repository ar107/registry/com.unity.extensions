﻿using UnityEngine;

public static class CameraExtensions
{
    public static Bounds GetOrthographicBounds(this Camera camera)
    {
        var screenAspect = (float)camera.pixelWidth / (float)camera.pixelHeight;
        var cameraHeight = camera.orthographicSize * 2;

        return new Bounds
        (
            camera.transform.position,
            new Vector3(cameraHeight * screenAspect, cameraHeight, 0)
        );
    }

    public static int GetPPU(this Camera camera)
    {
        return Mathf.RoundToInt((camera.pixelHeight / 2) / camera.orthographicSize);
    }
}
