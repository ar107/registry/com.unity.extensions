﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class BoundsExtensions
{
    public static bool IsWithin(this Bounds value, Bounds container)
    {
        var x1 = value.center.x - value.size.x / 2;
        var x2 = value.center.x + value.size.x / 2;
        var y1 = value.center.y - value.size.y / 2;
        var y2 = value.center.y + value.size.y / 2;

        var points = new[] 
        {
            new Vector2(x1, y1),
            new Vector2(x1, y2),
            new Vector2(x2, y2),
            new Vector2(x2, y1)
        };

        for (int i = 0; i < points.Length; i++)
            if (!container.Contains(points[i]))
            {
                Debug.LogWarning($"Bad point: {points[i]}");
                Debug.DrawLine(Vector3.zero, points[i], Color.cyan);
                return false;
            }

        return true;
    }

    public static Vector3[] GetPoints(this Bounds value, float z = 0)
    {
        var x1 = value.center.x - value.size.x / 2;
        var x2 = value.center.x + value.size.x / 2;
        var y1 = value.center.y - value.size.y / 2;
        var y2 = value.center.y + value.size.y / 2;

        return new[] 
        {
            new Vector3(x1, y1, z),
            new Vector3(x1, y2, z),
            new Vector3(x2, y2, z),
            new Vector3(x2, y1, z)
        };
    }
}