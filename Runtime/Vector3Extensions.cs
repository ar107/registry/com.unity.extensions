﻿using UnityEngine;

public static class Vector3Extensions
{
    public static Quaternion GetDirectionalAngle(this Vector3 position, Vector3 target)
    {
        var diff = target - position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0f, 0f, rot_z);
    }

    public static bool IsDistanceBigger(Vector3 a, Vector3 b, float value)
    {
        //Debug.Log($"Distance check: {a.ToString("F20")}->{b.ToString("F20")} // {(b - a).sqrMagnitude.ToString("F6")} : {(value * value).ToString("F6")} = {(b - a).sqrMagnitude > value * value}");

        return (b - a).sqrMagnitude > value * value;
    }

    public static bool IsDotBetween(Vector3 a, Vector3 b, Vector3 dot)
    {
        var distanceAB = (b - a).sqrMagnitude;
        var distanceAC = (dot - a).sqrMagnitude;
        var distanceBC = (dot - b).sqrMagnitude;

        return Mathf.Approximately(distanceAB, distanceAC + distanceBC);
    }

    public static Vector3 GetClosestPoint(Vector3 relative, Vector3[] points)
    {
        var closestIndex = -1;
        var closestDistance = float.PositiveInfinity;

        for (int i = 0; i < points.Length; i++)
        {
            var distance = Vector3.Distance(relative, points[i]);

            if (distance > closestDistance)
                continue;

            closestDistance = distance;
            closestIndex = i;
        }

        return points[closestIndex];
    }

    public static Vector3 Random()
    {
        return new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
    }

    public static Vector3 WithY(this Vector3 a, float y)
    {
        return new Vector3(a.x, y, a.z);
    }

    public static Vector3 WithZ(this Vector3 a, float z)
    {
        return new Vector3(a.x, a.y, z);
    }
}