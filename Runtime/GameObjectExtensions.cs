﻿using UnityEngine;

public static class GameObjectExtensions
{
    public static T GetComponentInTreeUp<T>(this GameObject gameObject) where T: Component
    {
        var result = gameObject.GetComponent<T>();

        if (result != null)
            return result;

        if (gameObject.transform.parent == null)
            return null;

        return gameObject.transform.parent.gameObject.GetComponentInTreeUp<T>();
    }
}
