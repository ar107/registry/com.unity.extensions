﻿using UnityEngine;

public static class ComponentExtensions
{
	public static T2 GetComponentInBranch<T1, T2>(this Component callerComponent, bool includeInactive = true) where T1 : Component where T2 : Component
	{
		T1[] rootComponents = callerComponent.transform.root.GetComponentsInChildren<T1>(includeInactive);

		if (rootComponents.Length == 0)
		{
			Debug.LogWarning($"Root component: No objects found with { typeof(T1).Name } component");
			return null;
		}

		for (int i = 0; i < rootComponents.Length; i++)
		{
			T1 rootComponent = rootComponents[i];

			// Is the caller a child of this root?
			if (!callerComponent.transform.IsChildOf(rootComponent.transform) && !rootComponent.transform.IsChildOf(callerComponent.transform))
				continue;

			T2 targetComponent = rootComponent.GetComponentInChildren<T2>(includeInactive);

			if (targetComponent == null)
				continue;

			return targetComponent;
		}

		return null;
	}

	public static T1 GetComponentHighestInBranch<T1>(this Component callerComponent, bool includeInactive = true) where T1 : Component
	{
		T1 result = null;

		var branch = callerComponent.transform;

		do
		{
			var tempResult = branch.GetComponent<T1>();

			branch = branch.parent;

			if (tempResult == null)
				continue;

			result = tempResult;
		}
		while (branch != null);

		return result;
	}

	public static T1 GetComponentInBranch<T1>(this Component callerComponent, bool includeInactive = true) where T1 : Component
	{
		return callerComponent.GetComponentInBranch<T1, T1>(includeInactive);
	}
}
