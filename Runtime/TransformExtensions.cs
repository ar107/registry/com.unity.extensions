﻿using System;
using UnityEngine;

public static class TransformExtensions
{
    public static void DestroyChilds(this Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            GameObject.Destroy(transform.GetChild(i).gameObject);
    }

    public static void DestroyChildsInEditor(this Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            GameObject.DestroyImmediate(transform.GetChild(i).gameObject);
    }

    public static bool IsLookAt(this Transform transform, Vector3 target, float accuracy = 1f)
    {
        var dirFromAtoB = (target - transform.position).normalized;
        var dotProd = Vector3.Dot(dirFromAtoB, transform.forward);

        if (dotProd > accuracy)
            return true;

        return false;
    }
    
    public static void ExecuteForHierarchy(this Transform transform, Action<Transform> action)
    {
        action.Invoke(transform);

        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).ExecuteForHierarchy(action);
    }
}
