﻿using System.Collections.Generic;

public static class ListExtensions
{
    public static T Pop<T>(this List<T> list)
    {
        if (list.Count == 0)
            throw new System.Exception("No objects in list");

        var value = list[0];
        list.RemoveAt(0);
        return value;
    }

    public static void Push<T>(this List<T> list, T value)
    {
        list.Insert(0, value);
    }

    public static T Random<T>(this IList<T> array)
    {
        if (array == null || array.Count == 0)
            return default(T);

        return array[UnityEngine.Random.Range(0, array.Count)];
    }
}