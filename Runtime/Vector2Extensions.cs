﻿using UnityEngine;

public static class Vector2Extensions
{
    public static Quaternion GetPlaneDirectionalAngle(this Vector3 position, Vector3 target)
    {
        var diff = new Vector2(target.x - position.x, target.z - position.z);
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0f, 0, rot_z);
    }

    public static Quaternion GetDirectionalAngle(this Vector2 position, Vector2 target)
    {
        var diff = target - position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        return Quaternion.Euler(0f, 0f, rot_z);
    }
}
