﻿using System;
using System.Threading;
using UnityEngine;

public static class Texture2DExtensions
{
	public enum SquareAngle
	{
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight
	}

	public static Color32[] FromColor(int size, Color32 color)
	{
		var colors = new Color32[size];

		for (int i = 0; i < colors.Length; i++)
			colors[i] = color;

		return colors;
	}

	public static Color32[] Fill(Color32[] colors, Color32 color)
    {
		for (int i = 0; i < colors.Length; i++)
		{
			if (colors[i].a == 0)
				continue;

			colors[i] = color;
		}

		return colors;
	}

	public static Color32[] Fill(Color32[] colors, Color color)
    {
		return Fill(colors, (Color32)color);
	}

    public static Color32[] GetTintedAlpha(Color32[] colors, Color32 tint)
    {
        var tint32 = (Color)tint;

		for (int i = 0; i < colors.Length; i++)
		{
			if (colors[i].a == 0)
				continue;

			colors[i] = Color32.Lerp(colors[i], tint32, colors[i].a / 255f);
		}

        return colors;
    }

	public static Color32[] GetTinted(Color32[] colors, Color32 tint)
	{
		var tint32 = (Color)tint;

		for (int i = 0; i < colors.Length; i++)
		{
			if (colors[i].a == 0)
				continue;

			colors[i] = Color32.Lerp(colors[i], tint32, .5f);
		}

		return colors;
	}

	public static Color32[] GetWhiteColored(Color32[] colors, Color32 tint)
	{
		var tint32 = (Color)tint;

		for (int i = 0; i < colors.Length; i++)
		{
			if (colors[i].a == 0)
				continue;

			if (colors[i].a == 1)
				colors[i] = tint;

			colors[i] = Color32.Lerp(colors[i], tint32, colors[i].r / 255f);

			//colors[i] = Color32.Lerp(colors[i], tint32, (colors[i].r + colors[i].b + colors[i].g) / (255f * 3f));
		}

		return colors;
	}

	public static Texture2D ResizeUp(Texture2D origin, int targetWidth, int targetHeight, SquareAngle angle)
	{
		var texture = new Texture2D(targetWidth, targetHeight, origin.format, false);

		texture.SetPixels32(Fill(texture.GetPixels32(), Color.white.WithA(0)));
		//texture.SetPixels32(Fill(texture.GetPixels32(), Color.white));

		switch (angle)
        {
			case SquareAngle.BottomLeft:
				texture.SetPixels(0, 0, origin.width, origin.height, origin.GetPixels());
				break;
			case SquareAngle.BottomRight:
				texture.SetPixels(targetWidth - origin.width, 0, origin.width, origin.height, origin.GetPixels());
				break;
			case SquareAngle.TopRight:
				texture.SetPixels(targetWidth - origin.width, targetHeight - origin.height, origin.width, origin.height, origin.GetPixels());
				break;
			case SquareAngle.TopLeft: 
				texture.SetPixels(0, targetHeight - origin.height, origin.width, origin.height, origin.GetPixels());
				break;
		}

		texture.Apply();

		return texture;
	}

	public static Color32[] Combine(Color32[] firstLevel, Color32[] secondLevel)
    {
		for (int i = 0; i < firstLevel.Length; i++)
			if (i >= secondLevel.Length)
				continue;
			else
				firstLevel[i] = Color32.Lerp(firstLevel[i], secondLevel[i], secondLevel[i].a / 255f);

        return firstLevel;
    }

	public static Color32[] SetOpacity(Color32[] colors, float opacity)
	{
		var opacityB = Convert.ToByte(opacity * 255);

        for (int i = 0; i < colors.Length; i++)
		{
			if (colors[i].a == 0)
				continue;

			colors[i].a = opacityB;
		}

		return colors;
	}

	public static Texture2D Copy(Texture2D origin)
    {
		var texture = new Texture2D(origin.width, origin.height, origin.format, false);

		var colors = origin.GetPixels();
		texture.SetPixels(colors);
		texture.Apply();

		return texture;
    }

	public static void DrawLine(this Texture2D tex, Vector2 p1, Vector2 p2, Color col)
	{
		Vector2 t = p1;
		float frac = 1 / Mathf.Sqrt(Mathf.Pow(p2.x - p1.x, 2) + Mathf.Pow(p2.y - p1.y, 2));
		float ctr = 0;

		while ((int)t.x != (int)p2.x || (int)t.y != (int)p2.y)
		{
			t = Vector2.Lerp(p1, p2, ctr);
			ctr += frac;
			tex.SetPixel((int)t.x, (int)t.y, col);
		}
	}


}

public class TextureScale
{
	public static Texture2D ResizeTexture(Texture2D originalTexture, int newWidth, int newHeight)
	{
		var material = new Material(Shader.Find("Unlit/Transparent"));
		material.SetTexture("_MainTex", originalTexture);  // or whichever the main texture property name is
														   // material.mainTexture = originalTexture; // or can do this instead with some materials
		return RenderMaterial(ref material, new Vector2Int(newWidth, newHeight));
	}

	public static Texture2D RenderMaterial(ref Material material, Vector2Int resolution)
	{
		var renderTexture = RenderTexture.GetTemporary(resolution.x, resolution.y);
		
		//Graphics.Blit(null, renderTexture, material);

		Texture2D texture = new Texture2D(resolution.x, resolution.y, TextureFormat.ARGB32, false)
        {
            filterMode = FilterMode.Bilinear,
            wrapMode = TextureWrapMode.Clamp
        };

        RenderTexture.active = renderTexture;

		GL.Clear(true, true, Color.clear);
		Graphics.Blit(null, renderTexture, material);

		texture.ReadPixels(new Rect(Vector2.zero, resolution), 0, 0);
		texture.Apply();

		RenderTexture.active = null;
		RenderTexture.ReleaseTemporary(renderTexture);

		return texture;
	}

	public class ThreadData
	{
		public int start;
		public int end;
		public ThreadData(int s, int e)
		{
			start = s;
			end = e;
		}
	}

	private static Color[] texColors;
	private static Color[] newColors;
	private static int w;
	private static float ratioX;
	private static float ratioY;
	private static int w2;
	private static int finishCount;
	private static Mutex mutex;

	public static void Point(Texture2D tex, int newWidth, int newHeight)
	{
		ThreadedScale(tex, newWidth, newHeight, false);
	}

	public static void Bilinear(Texture2D tex, int newWidth, int newHeight)
	{
		ThreadedScale(tex, newWidth, newHeight, true);
	}

	private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
	{
		texColors = tex.GetPixels();
		newColors = new Color[newWidth * newHeight];
		if (useBilinear)
		{
			ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
			ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
		}
		else
		{
			ratioX = ((float)tex.width) / newWidth;
			ratioY = ((float)tex.height) / newHeight;
		}
		w = tex.width;
		w2 = newWidth;
		var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
		var slice = newHeight / cores;

		finishCount = 0;
		if (mutex == null)
		{
			mutex = new Mutex(false);
		}
		if (cores > 1)
		{
			int i = 0;
			ThreadData threadData;
			for (i = 0; i < cores - 1; i++)
			{
				threadData = new ThreadData(slice * i, slice * (i + 1));
				ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
				Thread thread = new Thread(ts);
				thread.Start(threadData);
			}
			threadData = new ThreadData(slice * i, newHeight);
			if (useBilinear)
			{
				BilinearScale(threadData);
			}
			else
			{
				PointScale(threadData);
			}
			while (finishCount < cores)
			{
				Thread.Sleep(1);
			}
		}
		else
		{
			ThreadData threadData = new ThreadData(0, newHeight);
			if (useBilinear)
			{
				BilinearScale(threadData);
			}
			else
			{
				PointScale(threadData);
			}
		}

		tex.Reinitialize(newWidth, newHeight);
		tex.SetPixels(newColors);
		tex.Apply();

		texColors = null;
		newColors = null;
	}

	public static void BilinearScale(System.Object obj)
	{
		ThreadData threadData = (ThreadData)obj;
		for (var y = threadData.start; y < threadData.end; y++)
		{
			int yFloor = (int)Mathf.Floor(y * ratioY);
			var y1 = yFloor * w;
			var y2 = (yFloor + 1) * w;
			var yw = y * w2;

			for (var x = 0; x < w2; x++)
			{
				int xFloor = (int)Mathf.Floor(x * ratioX);
				var xLerp = x * ratioX - xFloor;
				newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
													   ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
													   y * ratioY - yFloor);
			}
		}

		mutex.WaitOne();
		finishCount++;
		mutex.ReleaseMutex();
	}

	public static void PointScale(System.Object obj)
	{
		ThreadData threadData = (ThreadData)obj;
		for (var y = threadData.start; y < threadData.end; y++)
		{
			var thisY = (int)(ratioY * y) * w;
			var yw = y * w2;
			for (var x = 0; x < w2; x++)
			{
				newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
			}
		}

		mutex.WaitOne();
		finishCount++;
		mutex.ReleaseMutex();
	}

	private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
	{
		return new Color(c1.r + (c2.r - c1.r) * value,
						  c1.g + (c2.g - c1.g) * value,
						  c1.b + (c2.b - c1.b) * value,
						  c1.a + (c2.a - c1.a) * value);
	}
}